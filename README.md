# Excalibur
### A website in Python

This website was created to serve as my portfolio and showcase my creations.

It's made in [_Python_](https://www.python.org/) and uses the _Flask_ micro-framework, along with several other libraries (listed below as Dependencies).

**It can be accessed on [vimino.net](https://www.vimino.net/).**

### Progress

- [x] Read
  - [x] About
  - [x] News
  - [x] Blog
- [x] Watch
  - [x] Projects
  - [x] Gallery
- [x] About
  - [x] This
  - [x] Me
    - [x] Contact

### To Do

- [x] Create Excalibur, the core module
- [x] Page (html) and Style (css)
- [x] Create Scabbard, the database manager
- [x] Deploy Page using WSGI
- [x] Implement Read/News (index)
- [x] Order News; Show date as "Today" when sensible
- [x] Create alternate color Styles (css)
- [x] Implement Read/About
- [x] Implement Watch/Gallery
- [x] Implement Watch/Projects
- [x] Create Whetstone, the style utility
- [x] Make a template and color it with separate styles
- [x] Add descriptions and other information to the database
- [x] Implement Read/Blog
- [x] Implement Know/Contact (requires emails and captcha)
- [x] Get Whetstone to also trim (minify) html and js files
- [x] Implement Know/Me
- [x] Implement Know/Experience
- [x] Implement Know/Education
- [x] Style the Error pages like the rest of the website (if possible)
- [x] Add comments to the Blog/Project/Gallery items
- [x] Join the Offline and Online Excaliburs (same contents)
- [x] Add a robots.txt file to keep crawlers away from /static/
- [x] Get Scabbard to export/import data instead of the "Migrate" command
- [x] Add a sitemap.xml file
- [x] Hotlink protection
- [x] Move the page Projects to the Static folder (avoids reprocessing)
- [x] Style all Source files (.py) according to [PEP8](https://www.python.org/dev/peps/pep-0008/) , using [linter-flake8](https://atom.io/packages/linter-flake8)
- [x] Add pagination to Comment sections
- [x] Add pagination to the News section
- [x] Add the controls (back, top, bottom) to the footer
- [x] Protection against [CSRF](https://en.wikipedia.org/wiki/Cross-site_request_forgery)
- [ ] Merge Excalibur and Scabbards' DB definitions
- [ ] Create Resources and separate from Projects/Gallery
- [ ] Add Categories
- [ ] Let the user filter the results
- [ ] Allow the usage of some BBCode on comments

### Dependencies

- [**Python 3**](https://www.python.org/downloads/) (language)
- [**Flask**](http://flask.pocoo.org/) (microframework)
- [**SQLAlchemy**](http://www.sqlalchemy.org/) (ORM)
- [**Flask-SQLAlchemy**](http://flask-sqlalchemy.pocoo.org/2.1/)
- [**Flask-Mail**](http://pythonhosted.org/Flask-Mail/)

### Version

- Excalibur: 0.9.2
- Scabbard: 0.9.17
- Whetstone: 0.6.0

### License

Copyright &copy; 2016-2019, Vítor "VIMinO" Martins

The source code is licensed under [The GNU General Public License](https://www.gnu.org/licenses/gpl.html),

and the media files under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/),
