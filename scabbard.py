#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Libraries

import sys  # Command line arguments
import re  # Regular Expressions

import sqlalchemy  # Database Abstraction
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime, Date, \
    SmallInteger, Boolean, ForeignKey  # , UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from datetime import datetime  # date operations
from enum import Enum  # enumeration on types of resource (URI)

import os  # to check if the source is a file
import json  # for importing JSON

from app.config import db, smtp  # Excaliburs' database configuration

# Email sending
import smtplib
from email.mime.text import MIMEText
from email.header import Header

# import traceback # Helprintpful for Debugging
# traceback.print_exc(limit = 1)
# traceback.print_exc(x)

###############################################################################

# Application configuration
App = {
    'name': 'Scabbard',
    'version': '0.9.17',
    'debug': True,
    'license':
    """Copyright (C) 2016 Vítor T. Martins

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.""",
}

# Regular Expression patterns
Pattern = {
    # Options
    'help': re.compile(r'-{0,2}h(elp)?', re.I),
    'version': re.compile(r'-{0,2}v(er(sion)?)?', re.I),
    'deploy': re.compile(r'-{0,2}d(ep(loy)?)?', re.I),
    'update': re.compile(r'-{0,2}u(p(date)?)?', re.I),
    'withdraw': re.compile(r'-{0,2}w(it(hdraw)?)?', re.I),
    'export': re.compile(r'-{0,2}(x|ex(port)?)', re.I),
    'import': re.compile(r'-{0,2}(m|im(port)?)', re.I),
    'list': re.compile(r'-{0,2}l(ist)?', re.I),
    'add': re.compile(r'-{0,2}a(dd)?', re.I),
    'edit': re.compile(r'-{0,2}e(d(it)?)?', re.I),
    'remove': re.compile(r'-{0,2}r(em(ove)?)?', re.I),
    'notify': re.compile(r'-{0,2}n(ot(ify)?)?', re.I),

    # Tables
    'news': re.compile(r'n(ews)?|r(ec(ord)?)?', re.I),
    'blog': re.compile(r'b(l(og)?)?|en(try)?', re.I),
    'gallery': re.compile(r'g(al(lery)?)?|ex(hibit)?', re.I),
    'projects': re.compile(r'p(roj(ects?)?)?', re.I),
    'comment': re.compile(r'c(om(ment)?)?', re.I),

    # Parameters
    'text': re.compile(r't(ext|itle)?=(?P<text>.*)', re.I),
    'date': re.compile(r'd(ate)?=(?P<date>.*)', re.I),
    'type': re.compile(r'(y|ty(pe)?)=(?P<type>.*)', re.I),
    'uri': re.compile(r'r(((e(source)?)?)|uri)=(?P<uri>.*)', re.I),
    'file': re.compile(r'(f(ile)?|l(ogo)?)=(?P<file>.*)', re.I),
    'preview': re.compile(r'p(re(view)?)?=(?P<preview>.*)', re.I),
    'body': re.compile(r'(de(sc(ription)?)?|b(o(dy)?)?)(=(?P<body>.*))?',
                       re.I),
    'user': re.compile(r'(u(s(er)?)?)(=(?P<user>.*))?', re.I),

    # Resource Types
    'page': re.compile(r'p(a(ge)?)?', re.I),
    'document': re.compile(r'f(i(le)?)?|d(oc(ument)?)?', re.I),
    'link': re.compile(r'l(i(nk)?)?|e(xt(ernal)?)?', re.I),
}

###############################################################################

# Database/Table sqlalchemy defenitions

Base = declarative_base()


class Record(Base):  # News/Record
    __tablename__ = 'Record'
    id = Column(Integer, primary_key=True)
    text = Column(String(256),  nullable=False)
    date = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Record %r>' % self.text


class Entry(Base):  # Blog/Entry
    __tablename__ = 'Entry'
    id = Column(Integer, primary_key=True)
    title = Column(String(256), unique=True, nullable=False)
    date = Column(DateTime, default=datetime.utcnow)
    views = Column(Integer, default=0)

    def __repr__(self):
        return '<Entry %r>' % self.text


class Exhibit(Base):  # Gallery/Exhibit
    __tablename__ = 'Exhibit'
    id = Column(Integer, primary_key=True)
    title = Column(String(128), unique=True, nullable=False)
    date = Column(Date, default=datetime.now().date())
    path = Column(String(128), nullable=False)
    preview = Column(String(128), nullable=True)
    description = Column(String(512), nullable=True)
    views = Column(Integer, default=0)
    # __table_args__ = (UniqueConstraint('exhibit_unicol', 'title'))

    def __repr__(self):
        return '<Exhibit %r>' % self.title


class ResourceType(Enum):
    none = 0
    page = 1  # An internal webpage
    document = 2  # An internal file
    link = 3  # An external webpage


class Project(Base):  # Projects/Project
    __tablename__ = 'Project'
    id = Column(Integer, primary_key=True)
    title = Column(String(128), unique=True, nullable=False)
    date = Column(Date, default=datetime.now().date())
    type = Column(SmallInteger, default=ResourceType.none.value)
    uri = Column(String(256), nullable=False)
    logo = Column(String(128), nullable=True)
    description = Column(String(512), nullable=True)
    views = Column(Integer, default=0)
    # __table_args__ = (UniqueConstraint('project_unicol', 'title'))

    def __repr__(self):
        return '<Project %r>' % self.title


class TableType(Enum):
    none = 0
    entry = 1  # Blog/Entry
    exhibit = 2  # Gallery/Exhibit
    project = 3  # Projects/Project


class Comment(Base):
    __tablename__ = 'Comment'
    id = Column(Integer, primary_key=True)
    table = Column(SmallInteger, default=TableType.none.value)
    user = Column(String(64), default='')
    email = Column(String(128), default='')
    text = Column(String(2048), nullable=False)
    date = Column(DateTime, default=datetime.now())
    ip = Column(String(64), default='')
    read = Column(Boolean, default=False)

    __mapper_args__ = {
        'polymorphic_on': table,
        'polymorphic_identity': 'comment',
        'with_polymorphic': '*'
    }


class EntryComment(Comment):
    # __tablename__ = 'EntryComment'
    # id = Column(Integer, ForeignKey(Comment.id), primary_key = True)
    entry = Column(Integer, ForeignKey(Entry.id))

    __mapper_args__ = {'polymorphic_identity': TableType.entry.value}


class ExhibitComment(Comment):
    # __tablename__ = 'ExhibitComment'
    # id = Column(Integer, ForeignKey(Comment.id), primary_key = True)
    exhibit = Column(Integer, ForeignKey(Exhibit.id))

    __mapper_args__ = {
        'polymorphic_identity': TableType.exhibit.value,
    }


class ProjectComment(Comment):
    # __tablename__ = 'ProjectComment'
    # id = Column(Integer, ForeignKey(Comment.id), primary_key = True)
    project = Column(Integer, ForeignKey(Project.id))

    __mapper_args__ = {
        'polymorphic_identity': TableType.project.value,
    }

###############################################################################


# Gets the substring after the last separator
def GetLastSubstring(string, separator='/'):
    if string is None:
        string = ''
    position = -1
    position = string.rfind(separator)
    if position < 0:
        print('Unable to get the last substring in (', string, ')')
        return ''
    return string[position+1:]


# Get the ResourceType Name from an Integer
def ResourceToString(type_id):
    type_str = ''
    for rt in ResourceType:
        if type_id == rt.value:
            type_str = rt.name
    if len(type_str) == 0:
        print('Unable to get the resource type name (', str(type_id), ')')
    return type_str


def StringToResource(type_str):
    type_id = 0
    for rt in ResourceType:
        if type_str == rt.name:
            type_id = rt.value
    if type_id == 0:
        print('Unable to get the resource type value (', type_str, ')')
    return type_id


# Get the TableType Name from an Integer
def TableToString(table):
    name = ''
    for tt in TableType:
        if table == tt.value:
            name = tt.name
    if len(name) == 0:
        print('Unable to get the table type name (', str(table), ')')
    return name


# Get the TableType Name from an Integer
def StringToTable(name):
    table = 0
    for tt in TableType:
        if name == tt.name:
            table = tt.value
    if table == 0:
        print('Unable to get the table type name (', name, ')')
    return table


# Get the Table class
#    table: The table name
def getTable(name=None):
    if re.match(Pattern['news'], name):
        return Record
    elif re.match(Pattern['blog'], name):
        return Entry
    elif re.match(Pattern['gallery'], name):
        return Exhibit
    elif re.match(Pattern['projects'], name):
        return Project
    elif re.match(Pattern['comment'], name):
        return Comment
    return None


# Establish a connection to the Database
#    full: specifies if the connection is to the server or a specific database
def Connect(full=True):
    if(App['debug']):
        if(full is True):
            print('Establishing a connection to the Database...')
        else:
            print('Establishing a connection to the Database Server...')

    try:
        path = db['path']
        if(full is True):
            path += '/' + db['name']
        engine = create_engine(path)
        return engine
    except Exception as x:
        print(x.__class__.__name__, '@Connect: ', x, sep='')
        exit(1)


# Show help text
#    args: Requests information about a specific command
def Help(args=[]):
    script = App['name'].lower()
    if len(args) == 0:
        print('Usage:', script, '[OPTION] [TABLE] [PARAMETERS]...')
        print('Manages the backend (database) for an Excalibur application.')
        print()
        print('OPTION')
        print('  -h   --help OPTION                    Show this information')
        print('  -d   --deploy                         Deploy the Database and \
              Tables')
        print('  -w   --withdraw                       Deletes the Database')
        print('  -x   --export                         Exports the Database')
        print('  -m   --import                         Imports the Database')
        print('  -l   --list TABLE                     List the entries of a \
              Table (or all if not specified)')
        print('  -a   --add TABLE PARAMETERS...        Add an item to the \
              Database')
        print('  -e   --edit TABLE ID PARAMETERS...    Edit an existing item \
              in the Database')
        print('  -r   --remove TABLE ID                Remove an item from \
              the Database (no ID means remove all)')
        print()
        print('TABLE')
        print('  n/r    news/record')
        print('  b/e    blog/entry')
        print('  g/x    gallery/exhibit')
        print('  p      project(s)')
    else:
        topic = args[0]
        if re.match(Pattern['list'], topic):
            print('Usage:  ')
            print('        ', script, '--list TABLE')
            print()
            print('Example:')
            print('        ', script, '--list news')
            print('        ', script, '--list blog')
            print('        ', script, '--list gallery')
            print('        ', script, '--list projects')
        elif re.match(Pattern['add'], topic):
            print('Usage:  ')
            print('        ', script, "--add news/record text='TEXT' \
                  date='DATE/DATETIME'")
            print('        ', script, "--add blog/entry title='TITLE' \
                  date='DATE/DATETIME'")
            print('        ', script, "--add gallery/exhibit title='TITLE' \
                  file='FILE_PATH' date='DATE' preview='PREVIEW_PATH'")
            print('        ', script, "--add projects/project title='TITLE' \
                  date='DATE' type='page/document/link' uri='file.html' \
                  logo='LOGO_PATH'")
            print()
            print('Example:')
            print('        ', script, "--add news text='Hello Year' \
                  date='30/01/2016 15:02:05'")
            print('        ', script, "--add blog text='Hello Year' \
                  date='30/01/2016 15:02:05'")
            print('        ', script, "--add gallery title='Drawing' \
                  file='drawing.png' year='2010'")
            print('        ', script, "--add projects title='Prototype' \
                  year='2003' type='page' uri='project.html' logo='proto.gif'")
        elif re.match(Pattern['edit'], topic):
            print('Usage:  ')
            print('        ', script, "--edit news/record ID text='TEXT' \
                  date='DATE/DATETIME'")
            print('        ', script, "--edit blog/entry ID title='TITLE' \
                  date='DATE/DATETIME'")
            print('        ', script, "--edit gallery/exhibit ID \
                  title='TITLE' file='FILE_PATH' date='DATE' \
                  preview='PREVIEW_PATH'")
            print('        ', script, "--edit projects/project ID \
                  title='TITLE' date='DATE' type='page/document/link' \
                  uri='file.html' logo='LOGO_PATH' \
                  description='description'")
            print()
            print('Example:')
            print('        ', script, "--edit journal 3 text='Goodbye Year' \
                  date='31/12/2015'")
            print('        ', script, "--edit gallery 7 title='Drawing++'")
            print('        ', script, "--edit project 4 title='Prototype 2' \
                  type='file' uri='project.zip' date='02-05-2012' \
                  logo='logo.png'")
        elif re.match(Pattern['remove'], topic):
            print('Usage:  ', script, '--remove TABLE ID')
            print()
            print('Example:', script, '--remove journal 3')
        else:
            Help()


# Show version and information
def Version():
    print(App['name'], App['version'])
    print('A utility to manage a Database, serving as the \
          back-end for Excalibur.')
    print()
    print(App['license'])


# Deploy\Update the tables in the Database
def Update():
    try:
        engine = Connect(True)

        if(App['debug']):
            print('Deploying table(s)...')

        Base.metadata.bind = engine
        Base.metadata.create_all(engine)

        if(App['debug']):
            print('Operation executed successfully')
    except Exception as x:
        print(x.__class__.__name__, '@Update: ', x, sep='')
        exit(2)


# Deploy the Database and its tables
def Deploy():
    engine = Connect(False)

    try:
        if(App['debug']):
            print('Deploying database...')

        # Deploy the Database

        connection = engine.connect()
        connection.execute('commit')

        # MySQL specific
        connection.execute('create database if not exists ' + db['name'] + ';')
        # connection.execute('create schema if not exists ' + db['name'] + ';')
        # MySQL specific

        connection.close()

        # then Deploy the Tables
        Update()
    except Exception as x:
        print(x.__class__.__name__, '@Deploy: ', x, sep='')
        exit(3)


# Remove the Database
def Withdraw():
    engine = Connect(False)

    try:
        confirm = input("Are you sure you want to \
delete the database (yes/no) ? ")

        if(confirm == 'y' or confirm == 'yes'):
            if(App['debug']):
                print('Erasing database...')

            connection = engine.connect()
            connection.execute('commit')

            # MySQL specific
            connection.execute('drop database if exists ' + db['name'] + ';')
            # MySQL specific

            if(App['debug']):
                print('Operation executed successfully')
        else:
            print("Operation canceled")
    except Exception as x:
        print(x.__class__.__name__, '@Withdraw: ', x, sep='')
        exit(4)


# Exports all the information to a JSON file
#    target: Optional filename (by default it prints to the terminal)
def Export(target=''):
    try:
        engine = Connect()
        Base.metadata.bind = engine

        Session = sessionmaker(bind=engine)
        session = Session()

        records = session.query(Record).all()
        entries = session.query(Entry).all()
        exhibits = session.query(Exhibit).all()
        projects = session.query(Project).all()
        comments = session.query(Comment).all()

        session.close()

        json = '{'

        if len(records) > 0:
            json += '"records":['
            i = 1
            j = len(records)
            for record in records:
                json += '{'
                json += '"text":"' + record.text.replace('"', '\\"') + '",'
                json += '"date":"' + str(record.date) + '"'
                json += '}'
                if i < j:
                    json += ','
                i += 1
            json += ']'

        if len(entries) > 0:
            if len(json) > 1:
                json += ','
            json += '"entries":['
            i = 1
            j = len(entries)
            for entry in entries:
                json += '{'
                json += '"title":"' + entry.title.replace('"', '\\"') + '",'
                json += '"date":"' + str(entry.date) + '",'
                json += '"views":"' + str(entry.views) + '"'
                json += '}'
                if i < j:
                    json += ','
                i += 1
            json += ']'

        if len(exhibits) > 0:
            if len(json) > 1:
                json += ','
            json += '"exhibits":['
            i = 1
            j = len(exhibits)
            for exhibit in exhibits:
                json += '{'
                json += '"title":"' + exhibit.title.replace('"', '\\"') + '",'
                json += '"date":"' + str(exhibit.date) + '",'
                json += '"path":"' + exhibit.path + '",'
                if exhibit.preview is not None and len(exhibit.preview) > 0:
                    json += '"preview":"' + exhibit.preview + '",'
                if exhibit.description is not None and \
                   len(exhibit.description) > 0:
                    json += '"description":"' + exhibit.description \
                            .replace('"', '\\"') \
                            .replace('<br />\n', "\\n") + '",'
                json += '"views":"' + str(exhibit.views) + '"'
                json += '}'
                if i < j:
                    json += ','
                i += 1
            json += ']'

        if len(projects) > 0:
            if len(json) > 1:
                json += ','
            json += '"projects":['
            i = 1
            j = len(projects)
            for project in projects:
                resource = project.uri
                if project.type == ResourceType.page or \
                   project.type == ResourceType.document:
                    resource = 'project/' + project.title.lower() + '/' + \
                               project.uri
                json += '{'
                json += '"title":"' + project.title.replace('"', '\\"') + '",'
                json += '"date":"' + str(project.date) + '",'
                json += '"type":"' + ResourceToString(project.type) + '",'
                json += '"uri":"' + resource + '",'
                if project.logo is not None and len(project.logo) > 0:
                    json += '"logo":"' + project.logo + '",'
                if project.description is not None and \
                   len(project.description) > 0:
                    json += '"description":"' + project.description \
                        .replace('"', '\\"').replace('<br />\n', "\\n") + '",'
                json += '"views":"' + str(project.views) + '"'
                json += '}'
                if i < j:
                    json += ','
                i += 1
            json += ']'

            if len(json) > 1:
                json += ','
                if len(comments) > 0:
                    json += '"comments":['
                    i = 1
                    j = len(comments)
            for comment in comments:
                parent_title = ''
                if comment.table == TableType.entry.value:
                    parent = session.query(Entry) \
                        .filter_by(id=comment.entry).one()
                    parent_title = parent.title
                elif comment.table == TableType.exhibit.value:
                    parent = session.query(Exhibit) \
                        .filter_by(id=comment.exhibit).one()
                    parent_title = parent.title
                elif comment.table == TableType.project.value:
                    parent = session.query(Project) \
                        .filter_by(id=comment.project).one()
                if parent is not None:
                    parent_title = parent.title
                else:
                    print('Unable to determine the comments associated \
table (', str(comment.type), ')', sep='')
                    continue
                json += '{'
                json += '"table":"' + TableToString(comment.table) + '",'
                json += '"user":"' + comment.user.replace('"', '\\"') + '",'
                json += '"text":"' + comment.text.replace('"', '\\"') \
                    .replace('<br />\n', "\\n") + '",'
                json += '"date":"' + str(comment.date) + '",'
                json += '"parent":"' + parent_title + '"'
                json += '}'
                if i < j:
                    json += ','
                i += 1
            json += ']'

        json += '}'

        if len(target) == 0:
            print(json)
        else:
            with open(target, "w") as output:
                output.write(json)
            print('Database exported to "', target, '"', sep='')
    except Exception as x:
        print(x.__class__.__name__, '@Export: ', x, sep='')
        exit(4)


# Imports a JSON file and populates the database with its contents
#    target: JSON or filename
def Import(source):
    try:
        string = ''
        if os.path.isfile(source):
            with open(source, "r") as source_file:
                string = source_file.read()
        else:
            string = source

        if len(string) == 0:
            print('There is no JSON to parse, is the string or file empty ?')
            exit(0)

        content = json.loads(string)

        # Make sure the Database exists
        Deploy()

        # Get ready to populate the Database
        engine = Connect()
        Base.metadata.bind = engine

        Session = sessionmaker(bind=engine)
        session = Session()

        try:
            if 'records' in content:
                for record in content['records']:
                    session.add(Record(text=record['text'],
                                date=record['date']))
                session.commit()
        except IntegrityError as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x.orig, sep='')
        except Exception as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x, sep='')

        try:
            if 'entries' in content:
                for entry in content['entries']:
                    session.add(Entry(title=entry['title'], date=entry['date'],
                                views=entry['views']))
                session.commit()
        except IntegrityError as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x.orig, sep='')
        except Exception as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x, sep='')

        try:
            if 'exhibits' in content:
                for exhibit in content['exhibits']:
                    preview = None
                    if 'preview' in exhibit:
                        preview = exhibit['preview']
                    description = None
                    if 'description' in exhibit:
                        description = exhibit['description'] \
                            .replace('\n', '<br />\n')
                    session.add(Exhibit(title=exhibit['title'],
                                date=exhibit['date'], path=exhibit['path'],
                                preview=preview, description=description,
                                views=exhibit['views']))
                session.commit()
        except IntegrityError as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x.orig, sep='')
        except Exception as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x, sep='')

        try:
            if 'projects' in content:
                for project in content['projects']:
                    resource_type = StringToResource(project['type'])
                    logo_path = None
                    if 'logo' in project:
                        logo_path = project['logo']
                    description = None
                    if 'description' in project:
                        description = project['description'] \
                            .replace('\n', '<br />\n')
                    session.add(Project(title=project['title'],
                                        date=project['date'],
                                        type=resource_type, uri=project['uri'],
                                        logo=logo_path,
                                        description=description,
                                        views=project['views']))
                session.commit()
        except IntegrityError as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x.orig, sep='')
        except Exception as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x, sep='')

        try:
            if 'comments' in content:
                for comment in content['comments']:
                    table = StringToTable(comment['table'])

                    parent_id = ''
                    if table == TableType.entry.value:
                        parent = session.query(Entry) \
                            .filter_by(title=comment['parent']).one()
                    elif table == TableType.exhibit.value:
                        parent = session.query(Exhibit) \
                            .filter_by(title=comment['parent']).one()
                    elif table == TableType.project.value:
                        parent = session.query(Project) \
                            .filter_by(title=comment['parent']).one()
                    if parent is None:
                        print('Unable to determine the comments associated \
                            table (', str(comment.type), ')', sep='')
                        continue
                    else:
                        parent_id = str(parent.id)

                    if table == TableType.entry.value:
                        session.add(EntryComment(table=table,
                                    user=comment['user'],
                                    text=comment['text']
                                    .replace('\n', '<br />\n'),
                                    date=comment['date'],
                                    entry=parent_id))
                    elif table == TableType.exhibit.value:
                        session.add(ExhibitComment(table=table,
                                    user=comment['user'], text=comment['text']
                                    .replace('\n', '<br />\n'),
                                    date=comment['date'], exhibit=parent_id))
                    elif table == TableType.project.value:
                        session.add(ProjectComment(table=table,
                                    user=comment['user'], text=comment['text']
                                    .replace('\n', '<br />\n'),
                                    date=comment['date'], project=parent_id))
                session.commit()
        except IntegrityError as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x.orig, sep='')
        except Exception as x:
            session.rollback()
            print(x.__class__.__name__, '@Import: ', x, sep='')

        session.close()
    except Exception as x:
        print(x.__class__.__name__, '@Import: ', x, sep='')
        exit(4)


# List the contents of a Table
#    table: The table from which to list the contents
#    subtable: The table to act as a filter (used for Comments)
def List(table=None, subtable=None):
    if table is None:
        List(Record)
        List(Entry)
        List(Exhibit)
        List(Project)
        List(Comment)
        exit(0)

    try:
        engine = Connect()
        Base.metadata.bind = engine

        Session = sessionmaker(bind=engine)
        session = Session()

        if(App['debug']):
            print('Listing', table.__name__, 'contents...')

        result = []
        if subtable is None:
            result = session.query(table).all()
        else:
            table_filter = TableType.none.value
            for tt in TableType:
                if str(tt.name) == subtable.__name__.lower():
                    table_filter = tt.value
                    break
            result = session.query(table) \
                .filter(Comment.table == table_filter).all()

        session.close()
        if(len(result) == 0):
            print("The table is empty.")
        else:
            liststr = ''

            if table.__name__ == 'Record':
                liststr += 'Record: Id | Text | Date'
                for record in result:
                    liststr += '\n'+str(record.id)+' | '+record.text+' | ' \
                               + str(record.date)
            elif table.__name__ == 'Entry':
                liststr += 'Entry: Id | Text | Date | View Count'
                for entry in result:
                    liststr += '\n'+str(entry.id)+' | '+entry.title+' | ' \
                               + str(entry.date)+' | '+str(entry.views)
            elif table.__name__ == 'Exhibit':
                liststr += 'Exhibit: Id | Title | Date | File Path | \
                           Preview Path | View Count'
                for exhibit in result:
                    liststr += '\n' + str(exhibit.id)+' | '+exhibit.title \
                                + ' | '+str(exhibit.date)+' | '+exhibit.path \
                                + ' | '

                    if exhibit.preview is not None:
                        liststr += exhibit.preview

                    liststr += ' | ' + str(exhibit.views)

                    if exhibit.description is not None:
                        liststr += ' |\n' + exhibit.description
            elif table.__name__ == 'Project':
                liststr += 'Project: Id | Title | Date | Type | URI | \
                           Logo Path | View Count | Description'
                for project in result:
                    type_name = ''
                    for rt in ResourceType:
                        if project.type == rt.value:
                            type_name = rt.name

                    liststr += '\n' + str(project.id)+' | '+project.title \
                               + ' | '+str(project.date)+' | '+type_name \
                               + ' | '+project.uri+' | '

                    if project.logo is not None:
                        liststr += project.logo

                    liststr += ' | ' + str(project.views)

                    if project.description is not None:
                        liststr += ' |\n' + project.description
            elif table.__name__ == 'Comment':
                liststr += 'Comment: Id | Table | User | Email | Text | Date'
                for comment in result:
                    table_name = 'Unknown'
                    for tt in TableType:
                        if comment.table == tt.value:
                            table_name = tt.name

                    user_name = 'Someone'
                    if comment.user is not None and len(comment.user) > 0:
                        user_name = comment.user

                    user_email = ''
                    if comment.email is not None and len(comment.email) > 0:
                        user_email = comment.email

                    liststr += '\n'+str(comment.id)+' | '+table_name+' | ' \
                               + user_name+' | '+user_email+' | ' \
                               + comment.text+' | ' \
                               + str(comment.date)
            if len(liststr) == 0:
                return

            print(liststr)
    except Exception as x:
        print(x.__class__.__name__, '@List: ', x, sep='')
        exit(5)


# Add a new entry to a Table
#    table: The table which will get the new row
#    args: The various details the row should have (specific to the table)
def Add(table=None, args=[]):
    argcount = len(args)

    if(table is None) or argcount < 1:
        Help('a')
        exit(0)

    engine = Connect()

    try:
        Base.metadata.bind = engine

        Session = sessionmaker(bind=engine)
        session = Session()

        if(App['debug']):
            print('Adding ', table.__name__, '...', sep='')

        if table == Record:
            record = None

            text = ''
            date_value = ''
            date = datetime.now()

            for arg in args:
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    text = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date_value = match.group('date')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            if len(date_value) > 0:
                try:
                    date = datetime.strptime(date_value, '%Y-%m-%d %H:%M:%S')
                except:
                    date = datetime.strptime(date_value, '%Y-%m-%d')
                    now = datetime.now()
                    date = date.replace(hour=now.hour, minute=now.minute,
                                        second=now.second)

                record = Record(text=text, date=date)
            else:
                record = Record(text=text)

            session.add(record)
        elif table == Entry:
            entry = None

            title = ''
            date_value = ''
            date = datetime.now()

            for arg in args:
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    title = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date_value = match.group('date')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            if len(date_value) > 0:
                try:
                    date = datetime.strptime(date_value, '%Y-%m-%d %H:%M:%S')
                except:
                    date = datetime.strptime(date_value, '%Y-%m-%d')
                    now = datetime.now()
                    date = date.replace(hour=now.hour, minute=now.minute,
                                        second=now.second)

                entry = Entry(title=title, date=date)
            else:
                entry = Entry(title=title)

            session.add(entry)

            # Create the base Entry file
            file_path = 'app/templates/read/entry/' + \
                title.lower().replace(' ', '-') + '.html'
            file_content = '{% extends "layout.min.html" %}\n'
            file_content += '{% block body %}\n'
            file_content += '\t<h1>{{ entry.title }}</h1>\n'
            file_content += '\t<h3>Subtitle</h3>\n'
            file_content += '\n'
            file_content += '\t<section class="panel">\n'
            file_content += '\t\t<section>\n'
            file_content += '\t\t\tThis is an example blog entry.\n'
            file_content += '\t\t</section>\n'
            file_content += '\n'
            file_content += '\t\t<section class="details">\n'
            file_content += '\t\t\tAdded on {{ entry.date.date() }} at ' + \
                '{{ entry.date.time() }}\n'
            file_content += '\t\t</section>\n'
            file_content += '\t</section>\n'
            file_content += '\t{% include "comment.min.html" with context %}\n'
            file_content += '{% endblock %}'

            with open(file_path, 'w+') as newEntry:
                newEntry.write(file_content)

            newEntry.close()
        elif table == Exhibit:
            title = ''
            date_value = ''
            date = datetime.now().date()
            file_path = ''
            preview_path = None
            has_desc = False
            description = ''
            desc_text = ''

            for arg in args:
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    title = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date_value = match.group('date')
                    continue
                match = re.match(Pattern['file'], arg)
                if match is not None:
                    file_path = match.group('file')
                    continue
                match = re.match(Pattern['preview'], arg)
                if match is not None:
                    preview_path = match.group('preview')
                    continue
                match = re.match(Pattern['body'], arg)
                if match is not None:
                    has_desc = True
                    body_text = match.group('body')
                    if body_text is not None:
                        desc_text = body_text.replace('\\n', '<br />\n')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            if len(title) == 0 or len(file_path) == 0:
                print('The title and file parameters are mandatory.')
                exit(0)

            # Read each line of the Description
            if has_desc is True and (desc_text is None or len(desc_text) == 0):
                print('Description (leave blank to stop):')
                lines = []
                while True:
                    line = input()
                    if len(line) > 0:
                        lines.append(line)
                    else:
                        break
                if len(lines) > 0:
                    description = '<br />\n'.join(lines)
            elif len(desc_text) > 0:
                description = desc_text

            if len(date_value) > 0:
                date = datetime.strptime(date_value, '%Y-%m-%d')

            folder = 'gallery/' + title.lower() + '/'

            file_path = folder + file_path
            if preview_path is not None:
                if len(preview_path) == 0:
                    preview_path = None
                else:
                    preview_path = folder + preview_path

            exhibit = Exhibit(title=title, date=date, path=file_path,
                              preview=preview_path, description=description)

            session.add(exhibit)
        elif table == Project:
            title = ''
            date_value = ''
            date = datetime.now().date()
            type_name = ''
            type_value = 0
            uri = ''
            logo_path = None
            has_desc = False
            description = ''
            desc_text = ''

            for arg in args:
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    title = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date_value = match.group('date')
                    continue
                match = re.match(Pattern['type'], arg)
                if match is not None:
                    type_name = match.group('type')
                    continue
                match = re.match(Pattern['uri'], arg)
                if match is not None:
                    uri = match.group('uri')
                    continue
                match = re.match(Pattern['file'], arg)
                if match is not None:
                    logo_path = match.group('file')
                    continue
                match = re.match(Pattern['body'], arg)
                if match is not None:
                    has_desc = True
                    body_text = match.group('body')
                    if body_text is not None:
                        desc_text = body_text.replace('\\n', '<br />\n')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            if len(title) == 0 or len(uri) == 0:
                print('The title, type and uri parameters are mandatory.')
                exit(0)

            # Read each line of the Description
            if has_desc is True and (desc_text is None or len(desc_text) == 0):
                print('Description (leave blank to stop):')
                lines = []
                while True:
                    line = input()
                    if len(line) > 0:
                        lines.append(line)
                    else:
                        break
                if len(lines) > 0:
                    description = '<br />\n'.join(lines)
            elif len(desc_text) > 0:
                description = desc_text

            if len(date_value) > 0:
                date = datetime.strptime(date_value, '%Y-%m-%d')

            if len(type_name) > 0:
                if re.match(Pattern['page'], type_name) is not None:
                    type_value = ResourceType.page.value
                elif re.match(Pattern['document'], type_name) is not None:
                    type_value = ResourceType.document.value
                elif re.match(Pattern['link'], type_name) is not None:
                    type_value = ResourceType.link.value
                else:
                    print('Unknown Resource Type (', type_name, ')', sep='')
                    exit(0)

            folder = 'project/' + title.lower() + '/'
            if type_value == 1 or type_value == 2:
                uri = folder + uri

            if logo_path is not None:
                if len(logo_path) == 0:
                    logo_path = None
                else:
                    logo_path = folder + logo_path

            project = Project(title=title, date=date, type=type_value,
                              uri=uri, logo=logo_path, description=description)
            session.add(project)
        else:
            session.close()
            exit(0)

        session.commit()
        session.close()

        if(App['debug']):
            print('Operation executed successfully')
    except IntegrityError as x:
        print(x.__class__.__name__, '@Add: ', x.orig, sep='')
        exit(0)
    except Exception as x:
        print(x.__class__.__name__, '@Add: ', x, sep='')
        exit(6)


# Edits a Table row
#    table: The table holding the row
#    identifier: The row identifier to be altered
#    args: The various details the row should have (specific to the table)
def Edit(table, identifier, args=[]):
    argcount = len(args)

    if table is None or argcount < 1:
        Help('e')
        exit(0)

    engine = Connect()

    try:
        Base.metadata.bind = engine

        Session = sessionmaker(bind=engine)
        session = Session()

        if(App['debug']):
            print('Editing ', table.__name__, ' ', identifier, '...', sep='')

        if table == Record:
            # Get all rows relating to that identifier (must only be one)
            record = session.query(table).filter(Record.id == identifier).one()

            text = ''
            date = ''

            # Parse arguments (arg='value') and effect changes
            for arg in args:
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    text = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date = match.group('date')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            # Update the values
            if len(text) > 0:
                record.text = text

            if len(date) > 0:
                try:
                    date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                except:
                    date = datetime.strptime(date, '%Y-%m-%d')
                    now = datetime.now()
                    date = date.replace(hour=now.hour, minute=now.minute,
                                        second=now.second)
                record.date = date
        elif table == Entry:
            # Get all rows relating to that identifier (must only be one)
            entry = session.query(table).filter(Entry.id == identifier).one()

            title = ''
            date_value = ''
            date = datetime.now()

            for arg in args:
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    title = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date_value = match.group('date')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            if len(title) > 0:
                entry.title = title

            if len(date_value) > 0:
                try:
                    date = datetime.strptime(date_value, '%Y-%m-%d %H:%M:%S')
                    entry.date = date
                except:
                    date = datetime.strptime(date_value, '%Y-%m-%d')
                    now = datetime.now()
                    date = date.replace(hour=now.hour, minute=now.minute,
                                        second=now.second)
                    entry.date = date
        elif table == Exhibit:
            # Get all rows relating to that identifier (must only be one)
            exhibit = session.query(table) \
                      .filter(Exhibit.id == identifier).one()

            title = ''
            date = ''
            file_path = ''
            preview_path = None
            has_desc = False
            description = ''

            for arg in args:
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    title = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date = match.group('date')
                    continue
                match = re.match(Pattern['file'], arg)
                if match is not None:
                    file_path = match.group('file')
                    continue
                match = re.match(Pattern['preview'], arg)
                if match is not None:
                    preview_path = match.group('preview')
                    continue
                match = re.match(Pattern['body'], arg)
                if match is not None:
                    has_desc = True
                    body_text = match.group('body')
                    if body_text is not None:
                        description = body_text.replace('\\n', '<br />\n')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            # Read each line of the Description
            if has_desc is True and (description is None or
                                     len(description) == 0):
                print('Description (leave blank to stop):')
                lines = []
                while True:
                    line = input()
                    if len(line) > 0:
                        lines.append(line)
                    else:
                        break
                if len(lines) > 0:
                    exhibit.description = '<br />\n'.join(lines)
                else:
                    exhibit.description = None
            elif description is not None and len(description) > 0:
                exhibit.description = description

            folder = 'gallery/'
            if len(title) > 0:
                folder += title.lower().replace(' ', '-') + '/'
                exhibit.title = title
            else:
                folder += exhibit.title.lower().replace(' ', '-') + '/'

            if len(date) > 0:
                exhibit.date = int(date)

            if len(title) > 0:
                if len(file_path) == 0:
                    exhibit.path = folder + GetLastSubstring(exhibit.path)
                else:
                    exhibit.path = folder + file_path

                if preview_path is None:
                    exhibit.preview = folder+GetLastSubstring(exhibit.preview)
                else:
                    if len(preview_path) > 0:
                        exhibit.preview = folder+preview_path
                    else:
                        exhibit.preview = None
            else:
                if len(file_path) > 0:
                    exhibit.path = folder + file_path

                if preview_path is not None:
                    if len(preview_path) > 0:
                        exhibit.preview = folder + preview_path
                    else:
                        exhibit.preview = None
        elif table == Project:
            # Get all rows relating to that identifier (must only be one)
            project = session.query(table) \
                      .filter(Project.id == identifier).one()

            title = ''
            date = ''
            type_name = ''
            uri = ''
            logo_path = None
            description = None
            has_desc = False

            for arg in args:
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    title = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date = match.group('date')
                    continue
                match = re.match(Pattern['type'], arg)
                if match is not None:
                    type_name = match.group('type')
                    continue
                match = re.match(Pattern['uri'], arg)
                if match is not None:
                    uri = match.group('uri')
                    continue
                match = re.match(Pattern['file'], arg)
                if match is not None:
                    logo_path = match.group('file')
                    # if len(logo_path) == 0:
                    #    project.logo = None
                    continue
                match = re.match(Pattern['body'], arg)
                if match is not None:
                    has_desc = True
                    body_text = match.group('body')
                    if body_text is not None:
                        description = body_text.replace('\\n', '<br />\n')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            # Read each line of the Description
            if has_desc is True and (description is None or
                                     len(description) == 0):
                print('Description (leave blank to stop):')
                lines = []
                while True:
                    line = input()
                    if len(line) > 0:
                        lines.append(line)
                    else:
                        break
                if len(lines) > 0:
                    project.description = '<br />\n'.join(lines)
                else:
                    project.description = None
            elif description is not None and len(description) > 0:
                exhibit.description = description

            if len(date) > 0:
                project.date = datetime.strptime(date, '%Y-%m-%d')

            folder = 'project/'
            if len(title) > 0:
                folder += title.lower().replace(' ', '-') + '/'
                project.title = title
            else:
                folder += project.title.lower().replace(' ', '-') + '/'

            if len(type_name) > 0:
                if re.match(Pattern['page'], type_name) is not None:
                    project.type = ResourceType.page.value
                elif re.match(Pattern['document'], type_name) is not None:
                    project.type = ResourceType.document.value
                elif re.match(Pattern['link'], type_name) is not None:
                    project.type = ResourceType.link.value
                else:
                    print('Unknown Resource Type (', type_name, ')', sep='')
                    exit(0)

            if len(title) > 0:
                if len(uri) > 0:
                    if project.type == ResourceType.page.value or \
                       project.type == ResourceType.document.value:
                        project.uri = folder + uri
                    else:
                        project.uri = uri
                else:
                    if project.type == ResourceType.page.value or \
                       project.type == ResourceType.document.value:
                        project.uri = folder + GetLastSubstring(project.uri)

                if logo_path is None:
                    project.logo = folder + GetLastSubstring(project.logo)
                else:
                    if len(logo_path) > 0:
                        project.logo = folder + logo_path
                    else:
                        project.logo = None
            else:
                if len(uri) > 0:
                    if project.type == ResourceType.page.value or \
                       project.type == ResourceType.document.value:
                        project.uri = folder + uri
                    else:
                        project.uri = uri

                if logo_path is not None:
                    if len(logo_path) > 0:
                        project.logo = folder + logo_path
                    else:
                        project.logo = None
        elif table == Comment:
            # Get all rows relating to that identifier (must only be one)
            comment = session.query(table) \
                      .filter(Comment.id == identifier).one()
            # comment = session.query(table).filter((target.__class__) \
            # .id == identifier).one()
            # print(comment)

            user = ''
            text = ''
            date = ''

            # Parse arguments (arg='value') and effect changes
            for arg in args:
                match = re.match(Pattern['user'], arg)
                if match is not None:
                    user = match.group('user')
                    continue
                match = re.match(Pattern['text'], arg)
                if match is not None:
                    text = match.group('text')
                    continue
                match = re.match(Pattern['date'], arg)
                if match is not None:
                    date = match.group('date')
                    continue
                print('Unknown argument (' + arg + ')')
                exit(0)

            # Update the values
            if len(user) > 0:
                comment.user = user

            if len(text) > 0:
                comment.text = text

            if len(date) > 0:
                try:
                    date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                except:
                    date = datetime.strptime(date, '%Y-%m-%d')
                    now = datetime.now()
                    date = date.replace(hour=now.hour, minute=now.minute,
                                        second=now.second)
                comment.date = date
        else:
            session.close()
            exit(0)

        session.commit()
        session.close()

        if(App['debug']):
            print('Operation executed successfully')
    except MultipleResultsFound as x:
        # print("MultipleResultsFound@Edit: ", x)
        print('That identifier is associated with several entries.')
        exit(0)
    except NoResultFound as x:
        # print("NoResultFound@Edit: ", x)
        print('That identifier is not associated')
        exit(0)
    except Exception as x:
        # traceback.print_exc(x)
        print(x.__class__.__name__, '@Edit: ', x, sep='')
        exit(7)


# Remove a Table row
#    table: The table containing the row
#    identifier: The identifier of the row to be removed
def Remove(table, identifier=''):
    engine = Connect()

    try:
        Base.metadata.bind = engine

        Session = sessionmaker(bind=engine)
        session = Session()

        if(App['debug']):
            if len(identifier) > 0:
                print('Removing', table.__name__, identifier)
            else:
                print('Removing every', table.__name__)

        if len(identifier) > 0:
            target = session.query(table) \
                     .filter(table.id == identifier).first()
            if target is None:
                print('That identifier is not associated')
            else:
                session.query(table).filter(table.id == identifier).delete()
                session.commit()
                print('Operation executed successfully')
        else:
            confirm = input("Are you sure you want to delete them (yes/no) ? ")

            if(confirm == 'y' or confirm == 'yes'):
                session.query(table).delete()
                session.commit()

                if(App['debug']):
                    print('Operation executed successfully')
            else:
                if(App['debug']):
                    print('Operation canceled')

        session.close()
    except Exception as x:
        print(x.__class__.__name__, '@Remove: ', x, sep='')
        exit(11)

###############################################################################


# Main application
if __name__ == '__main__':
    # The first argument is always the scripts name so
    # any less than 2 means "without arguments"
    if len(sys.argv) < 2:
        sys.argv.append('help')

    # Start a counter for the identifiers and a "loop breaking" variable
    i = 1
    loop = True
    # Loop until we're done with the arguments
    while loop:
        # Get the first argument (after sys.argv[0], the script name)
        arg = sys.argv[i]

        # Match the command (send further arguments when available)
        if re.match(Pattern['help'], arg):
            Help(sys.argv[i+1:])  # Run Help with any further arguments
            exit(0)
        elif re.match(Pattern['version'], arg):
            Version()
            exit(0)
        elif re.match(Pattern['withdraw'], arg):
            Withdraw()
            exit(0)
        elif re.match(Pattern['update'], arg):
            Update()
            exit(0)
        elif re.match(Pattern['deploy'], arg):
            Deploy()
            exit(0)
        elif re.match(Pattern['export'], arg):
            target = ''
            i += 1
            if i < len(sys.argv):
                target = sys.argv[i]

            Export(target)
            exit(0)
        elif re.match(Pattern['import'], arg):
            source = ''
            i += 1
            if i < len(sys.argv):
                source = sys.argv[i]
            else:
                print('You need to specify a JSON string or file')
                exit(0)

            Import(source)
            exit(0)
        elif re.match(Pattern['notify'], arg):
            engine = Connect()
            Base.metadata.bind = engine

            Session = sessionmaker(bind=engine)
            session = Session()

            newComments = session.query(Comment).filter_by(read=False).all()
            commentCount = 0
            try:
                commentCount = len(newComments)
            except:
                print('Unable to count the Comments.')
                exit(0)

            if commentCount == 0:
                print('No unread comments.')
                exit(0)

            subject = "You have " + str(commentCount) + \
                      " unread comment(s)."

            message_text = ''

            for nc in newComments:
                table_name = 'Unknown'
                for tt in TableType:
                    if nc.table == tt.value:
                        table_name = tt.name

                table_row = -1
                if nc.table == TableType.entry.value:
                    table_row = nc.entry
                elif nc.table == TableType.exhibit.value:
                    table_row = nc.exhibit
                elif nc.table == TableType.project.value:
                    table_row = nc.project

                user_name = 'Someone'
                if nc.user is not None and len(nc.user) > 0:
                    user_name = nc.user

                message_text += 'Comment ' + str(nc.id)
                message_text += ' (' + table_name + ', ' + \
                                str(table_row) + ') '
                message_text += 'on ' + str(nc.date) + ', '
                message_text += user_name
                if nc.ip is not None and len(nc.ip) > 0:
                    message_text += ' (' + str(nc.ip) + ')'
                message_text += ' said\n' + nc.text

                message_text += '\n'

                nc.read = True

            message = MIMEText(message_text, 'plain', 'utf-8')
            message['Subject'] = Header(subject, 'utf-8')

            message['From'] = smtp['noreply']
            message['To'] = ', '.join(smtp['recipients'])

            success = 'was sent.'
            try:
                # print(message_text)

                mail = smtplib.SMTP('localhost')
                mail.sendmail(smtp['noreply'], smtp['recipients'],
                              message.as_string())
                mail.quit()

                # Success means we update the new comments to "read"
                session.commit()
            except Exception as x:
                success = 'failed.\nException: ' + str(x)

            report = 'Notification about ' + str(commentCount) + \
                     ' unread comment(s) on ' + str(datetime.now()) + \
                     ' ' + success + '\n'

            print(report)
            with open('log.txt', 'a') as log:
                log.write(report)

        elif re.match(Pattern['list'], arg):
            table_name = ''

            i += 1
            if i < len(sys.argv):
                table_name = sys.argv[i]

            table = None
            subtable = None

            # Get the Table (second argument)
            table = getTable(table_name)
            if table == Comment:
                # Get the SubTable (third argument)
                i += 1
                if i < len(sys.argv):
                    subtable_name = sys.argv[i]
                    subtable = getTable(subtable_name)
                    if subtable is None:
                        print('Unknown table (', subtable_name, ')', sep='')
                        exit(0)
                elif table is None:
                    print('Unknown table (', table_name, ')', sep='')
                    exit(0)

            if subtable is None:
                List(table)
            else:
                List(table, subtable)
            exit(0)
        else:  # Requires Table
            i += 1
            if i >= len(sys.argv):
                print(App['name'] + ': You must specify a table.')
                exit(0)

            table_name = sys.argv[i]
            table = None

            # Get the Table (second argument) since further cases require it
            table = getTable(table_name)
            if table is None:
                print("Unknown table (", table_name, ")", sep='')
                exit(0)

            if re.match(Pattern['add'], arg):
                Add(table, sys.argv[i+1:])
                i = len(sys.argv)
            elif re.match(Pattern['remove'], arg):
                identifier = ''
                i += 1
                if i < len(sys.argv):
                    identifier = sys.argv[i]

                Remove(table, identifier)
                i = len(sys.argv)
            else:  # Requires Id
                # Get the Identifier (third argument) since
                # further cases require it
                i += 1
                if i >= len(sys.argv):
                    print(App['name'], ': You must specify an identifier.',
                          sep='')
                    exit(0)

                identifier = sys.argv[i]

                if re.match(Pattern['edit'], arg):
                    Edit(table, identifier, sys.argv[i+1:])
                    i = len(sys.argv)
                else:
                    # If it wasn't any of the accepted commands
                    # just indicate the error
                    print(App['name'], ': Unknown command (', arg, ')', sep='')

        # Increment the argument count and set the loop up for breaking
        # if it was the last one
        i += 1
        if i >= len(sys.argv):
            loop = False
