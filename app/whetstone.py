#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys  # System : argv
import re  # Regular Expressions
import json  # For the Color.json files

#######################################################################

App = {
    'name': 'Whetstone',
    'version': '0.6.0',
    'license':
    """Copyright (C) 2016 Vítor T. Martins

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.""",
}

Pattern = {
    'help': re.compile(r'-{0,2}h(elp)?', re.I),
    'version': re.compile(r'-{0,2}v(er(sion)?)?', re.I),
    'paint': re.compile(r'-{0,2}p(a(int))?', re.I),
    'trim': re.compile(r'-{0,2}t(r(im)?)?', re.I),

    'extension': re.compile(r'.+\.(?P<extension>[^.]+)$', re.I),
    'script': re.compile(r'<script .*?>(?P<script>.*?)</script>', re.I),
    'style': re.compile(r'<style .*?>(?P<style>.*)</style>', re.I),

    'prefiller': re.compile(r'^\s+'),
    'posfiller': re.compile(r'\s+$'),
    'sincom': re.compile(r'//.*\n'),  # Single-line Comment
    'mulcom': re.compile(r'(\/\*.*?\*(?=\/)\/)|(<!--.*?-(?=->)->)',
                         flags=re.DOTALL),  # Multi-line Comment
    'newline': re.compile(r'\n')
}

#######################################################################


def Help():
    print('--help                          Show this information')
    print('--paint <template> <colors>     Create a css file from \
          a template (.css) and a list of colors (.json)')
    print('--trim <file>                   \
          Minify a file (html, css, js, json)')


def Version():
    print(App['name'], App['version'])
    print('A utility to manage css files.')
    print()
    print(App['license'])


def Trim(path):
    extension = ''
    match = re.match(Pattern['extension'], path)
    if match is not None:
        extension = match.group('extension').lower()
        if extension is None or len(extension) == 0:
            print(App['name'], ': Unknown file extension (', path, ').',
                  sep='')
            return
    else:
        print(App['name'], ': Unknown file extension (', path, ').', sep='')
        return

    with open(path, 'r') as f:
        target = f.read()

    # Flags
    #      re.I = ignore case (upper/lower)
    #      re.M = apply to all lines (for multi-line stuff)
    #      re.DOTALL = '.' matches anything (including '\n')

    if extension == 'css':
        # Remove comments
        target = re.sub(Pattern['mulcom'], '', target)

        # Remove all filler before and after the line
        target = re.sub(Pattern['prefiller'], '', target)
        target = re.sub(Pattern['posfiller'], '', target)

        # Remove all Spaces/Tabs around special characters
        target = re.sub(r'\s*([:;{}])\s*', '\g<1>', target)

        # Remove newlines
        target = re.sub(Pattern['newline'], '', target)
    elif extension == 'js':
        # Remove comments
        target = re.sub(Pattern['sincom'], '', target)
        target = re.sub(Pattern['mulcom'], '', target)

        # Remove all filler before and after the line
        target = re.sub(Pattern['prefiller'], '', target)
        target = re.sub(Pattern['posfiller'], '', target)

        # Remove all filler around special characters
        target = re.sub(r'([\(\[\{])\s*(.*?)\s*([\)\]\}])', '\g<1>\g<2>\g<3>',
                        target, flags=re.DOTALL)
        target = re.sub(r'\s*([,;+-=<>]|&+|\|+)\s*', '\g<1>', target)
        target = re.sub(r'([\)\]\}])\s+([\(\[\{])', '\g<1>\g<2>',
                        target)
        target = re.sub(r'\s*([\)\]\}])\s*', '\g<1>', target)
        target = re.sub(r'\s*([\(\[\{])\s*', '\g<1>', target)

        # Remove newlines
        target = re.sub(Pattern['newline'], '', target)
    elif extension == 'json':
        # Remove all spaces/tabs before and after the line
        target = re.sub(Pattern['prefiller'], '', target)
        target = re.sub(Pattern['posfiller'], '', target)

        # Remove all filler around special characters
        target = re.sub(r'[ \t]*([:;\{\}\[\]])[ \t]*', '\g<1>', target)

        # Remove newlines
        target = re.sub(Pattern['newline'], '', target)
    elif extension == 'html':
        # found_match = True
        # while found_match == True:
        #    match = re.match(Pattern[''], target)
        #    if match != None:

        # Remove comments
        target = re.sub(Pattern['mulcom'], '', target)

        # Remove all filler before and after the line
        target = re.sub(Pattern['prefiller'], '', target)
        target = re.sub(Pattern['posfiller'], '', target)

        # Remove all filler around special characters
        target = re.sub(r'I([^\s])\s+([<>])\s+([^\s])?', '\g<1>\g<2>\g<3>',
                        target)
        target = re.sub(r'\>\s+\<', '><', target)

        # Remove Jinja constructs: {% ... %} and {{ ... }}
        target = re.sub(r'\s+(\{[{%])', '\g<1>', target)
        target = re.sub(r'([}%]\})\s+', '\g<1>', target)

        # Remove newlines
        target = re.sub(Pattern['newline'], '', target)
    else:
        print(App['name'], ': Unsupported extension (', extension, ').',
              sep='')
        return

    # Print the minified Css
    print(target)


def Paint(templatePath, colorPath):
    # Load the Template.css file and read its contents
    with open(templatePath, 'r') as t:
        template = t.read()

    # Load and decode Colors.json into a dictionary
    with open(colorPath, 'r') as c:
        color = json.load(c)

    # For each Color replace the matching name with the color
    for name in color:
        template = re.sub('§'+name+'§', color[name], template, flags=re.M)

    # Set the unmatched colors as black
    template = re.sub(r'§[^§]*§', '#000000', template, flags=re.M)

    # Print the resulting Css removing the final \n
    print(template[:-1])

#######################################################################

# Template.css example:


"""
nav ul li{
    background:§backcolor§;
    color:§frontcolor§;
}
"""

# Color.json example:


"""
{
    "backcolor":"#555555",
    "frontcolor":"#ffffff",
}
"""

#######################################################################

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.argv.append('help')

    i = 1
    loop = True
    while loop:
        arg = sys.argv[i]

        if re.match(Pattern['help'], arg):
            Help()
            exit()
        elif re.match(Pattern['version'], arg):
            Version()
            exit()
        elif re.match(Pattern['paint'], arg):
            i += 2
            if i == len(sys.argv):
                print(App['name'] + ': You must specify a template \
                      and color files.')
                exit(0)

            template = sys.argv[i-1]
            colors = sys.argv[i]
            Paint(template, colors)
        elif re.match(Pattern['trim'], arg):
            i += 1
            if i == len(sys.argv):
                print(App['name'] + ': You must specify the file \
                      you want to trim.')
                exit(0)

            path = sys.argv[i]
            Trim(path)
        else:
            print(App['name'] + ': Unknown command (' + arg + ').')
            exit(0)

        i += 1
        if i >= len(sys.argv):
            loop = False
