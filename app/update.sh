#!/bin/bash

echo -n "updating: "
script=./whetstone.py

# Update the Style

echo -n "style.css "
$script --trim static/css/style.css > static/css/style.min.css

# Update Script(s)

echo -n "oneforall.js"
$script --trim static/script/oneforall.js > static/script/oneforall.min.js

# Update the Colored Template

regex="template/([^.]+).json"

for FILE in $(ls static/css/template/*.json)
do
	if [[ $FILE =~ $regex ]]
	then
		name="${BASH_REMATCH[1]}"
		echo -n "$name.css "
		$script --paint static/css/template/template.css static/css/template/$name.json  > static/css/$name.css
		$script --trim static/css/$name.css > static/css/$name.min.css
	fi
done

# Minify the main pages

echo -n "layout.html "
$script --trim templates/layout.html > templates/layout.min.html

echo -n "error.html "
$script --trim templates/error.html > templates/error.min.html

echo -n "hotlink.html "
$script --trim templates/hotlink.html > templates/hotlink.min.html

echo -n "comment.html "
$script --trim templates/comment.html > templates/comment.min.html

echo -n "nomatch.html "
$script --trim templates/nomatch.html > templates/nomatch.min.html

echo
