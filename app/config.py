# -*- coding: utf-8 -*-

import random

# Global Database Settings
db = {
    # MySQL path = mysql+mysqldb://<user>:<password>@<host>[:<port>]/<dbname>
    # This is all I have to update when pushing config up
    'path': 'mysql+mysqldb://root:root@localhost',
    'name': 'Excalibur',
}

smtp = {
    'server': 'mail.domain',
    'user': 'user@domain',
    'pass': "password",
    'anonymous': 'anonymous@domain',
    'noreply': 'noreply@domain',
    'recipients': ['user@domain']
}


class Challenge:
    def script():
        challenge = '<script>Setup();</script>'
        return challenge

    def noscript():
        challenge = ''
        challenge += '<noscript>\n'
        challenge += "I'm human&nbsp;"
        challenge += '<input type="checkbox" name="gotcha" />'
        challenge += '</noscript>\n'

        challenge += '<input type = "hidden" id = "email" name = "email"' + \
                     ' value = "' + str(random.randrange(1, 1000)) + '" />\n'
        return challenge

    def reother():
        return str(random.randrange(0, 1000000))

    def another(other):
        element = '<input type = "hidden" id = "other" name = "other"' + \
            ' value = "' + str(other) + '" />'
        return element

    def get(other=None):
        string = '<span class = "stubby">'
        if other is not None:
            string += Challenge.another(other)
        string += Challenge.script()
        string += Challenge.noscript()
        string += '</span>'
        return string

    def validate(post):
        gotcha = ''
        if 'gotcha' in post:
            gotcha = post.get('gotcha')

        email = ''
        if 'email' in post:
            email = post.get('email')

        try:
            if int(email) > 0 and len(gotcha) == 0:
                return True
        except:
            pass

        if len(gotcha) > 0:
            return True

        return False

    def getSecret():
        return str(random.randrange(0, 1000000))
