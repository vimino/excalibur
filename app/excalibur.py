#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016 Vítor T. Martins

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Excalibur

from config import db, smtp, Challenge

# Libraries

from flask import Flask, render_template, request, url_for
from flask import Markup, redirect, make_response, session, abort
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from enum import Enum

# Pagination
from math import ceil

# Send Emails
from flask_mail import Mail, Message
# Parse User/Email
import re
Pattern = {
    'name+email':
    re.compile(r'(?P<name>.+?)\s*\((?P<email>\w+(\.\w+)*@\w+(\.\w+)*)\)'),
    'email': re.compile(r'(?P<email>(?P<name>\w+(\.\w+)*)@\w+(\.\w+)*)')
}
# import traceback

# Functions


def getCopyrightYear():
    current_year = datetime.now().year
    if current_year != 2016:
        return ('2016 - ' + str(current_year))
    return '2016'


def getIP(request):
    # return str(request.remote_addr)
    return str(request.environ['REMOTE_ADDR'])


def getUser(sender):
    match = re.match(Pattern['name+email'], sender)
    if match is not None:
        return match.group('name')
    match = None
    match = re.match(Pattern['email'], sender)
    if match is not None:
        return match.group('name')
    return sender


def getEmail(sender):
    match = re.match(Pattern['name+email'], sender)
    if match is not None:
        return match.group('email')
    match = None
    match = re.match(Pattern['email'], sender)
    if match is not None:
        return match.group('email')
    return ''


def makeToken(force=False):
    if not force and 'token' not in session:
        session['token'] = Challenge.reother()

    # app.jinja.env.globals['token'] = session['token']
    return session['token']


def checkToken(token):
    match = session['token']  # session.pop('token', None)

    if token is None or match is None or token != match:
        raise Exception(str(token) + " != " + str(match))
        abort(403)


def clearToken():
    session.pop('token', None)


# Application


App = {
    'name': 'Excalibur',
    'version': '0.9.2',
    'per_page': 5,
}

app = Flask(__name__)
app.year = getCopyrightYear()
app.config.from_object(__name__)
app.config['PREFERRED_URL_SCHEME'] = 'https'
# app.config['SERVER_NAME'] = 'www.vimino.net' # Uncomment this when deploying
app.config['SECRET_KEY'] = Challenge.getSecret()

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = db['path'] + '/' + db['name']
db = SQLAlchemy(app)

app.config['MAIL_SERVER'] = smtp['server']
app.config['MAIL_PORT'] = 25
app.config['MAIL_USE_TLS'] = True
# MAIL_USE_SSL : default False
app.config['MAIL_USERNAME'] = smtp['user']
app.config['MAIL_PASSWORD'] = smtp['pass']
app.config['MAIL_SUPPRESS_SEND'] = False
mail = Mail(app)

# Functions


def url_for_page(page):
    args = request.view_args.copy()
    args['page'] = page
    return url_for(request.endpoint, **args)


app.jinja_env.globals['url_for_page'] = url_for_page


# Pagination
# http://flask.pocoo.org/snippets/44/

class Pagination(object):
    def __init__(self, page, per_page, total_pages):
        self.page = page
        self.per_page = per_page
        self.total_pages = total_pages

    @property
    def pages(self):
        return int(ceil(self.total_pages / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    def iter_pages(self, left_edge=1, left_current=3, right_current=4,
                   right_edge=1):
        last = 0
        for num in range(1, self.pages + 1):
            if num <= left_edge or \
                (num > self.page - left_current - 1 and
                    num < self.page + right_current) or \
                    num > self.pages - right_edge:
                    if last + 1 != num:
                        yield None
                    yield num
                    last = num


# Database Entities


class Record(db.Model):  # News/Record
    __tablename__ = 'Record'
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(1024),  nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, id, text, date):
        self.id = id
        self.text = text
        self.date = date

    def __repr__(self):
        return '<Record %r>' % self.text


class Entry(db.Model):  # Blog/Entry
    __tablename__ = 'Entry'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256), unique=True, nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    views = db.Column(db.Integer, default=0)

    def __init__(self, id, text, date):
        self.id = id
        self.text = text
        self.date = date

    def __repr__(self):
        return '<Entry %r>' % self.text


class Exhibit(db.Model):  # Gallery/Exhibit
    __tablename__ = 'Exhibit'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128),  nullable=False)
    date = db.Column(db.Date, default=datetime.now().date())
    path = db.Column(db.String(128), nullable=False)
    preview = db.Column(db.String(128), nullable=True)
    description = db.Column(db.String(512), nullable=True)
    views = db.Column(db.SmallInteger, default=0)

    def __init__(self, id, title, date, path, preview=None, description=None):
        self.id = id
        self.title = title
        self.date = date
        self.path = path
        self.preview = preview
        self.description = description

    def __repr__(self):
        return '<Exhibit %r>' % self.title


class ResourceType(Enum):
    page = 1  # An internal webpage
    document = 2  # An internal file
    link = 3  # An external webpage


class Project(db.Model):  # Project/Product
    __tablename__ = 'Project'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128),  nullable=False)
    date = db.Column(db.Date, default=datetime.now().date())
    type = db.Column(db.SmallInteger, default=ResourceType.document.value)
    uri = db.Column(db.String(256), nullable=False)
    logo = db.Column(db.String(128), nullable=True)
    description = db.Column(db.String(512), nullable=True)
    views = db.Column(db.Integer, default=0)

    def __init__(self, id, title, date, type_id, uri,
                 logo=None, description=None):
        self.id = id
        self.title = title
        self.date = date
        self.type = type_id
        self.uri = uri
        self.logo = logo
        self.description = description

    def __repr__(self):
        return '<Product %r>' % self.title


class TableType(Enum):
    none = 0
    entry = 1  # Blog/Entry
    exhibit = 2  # Gallery/Exhibit
    project = 3  # Projects/Project


class Comment(db.Model):
    __tablename__ = 'Comment'
    id = db.Column(db.Integer, primary_key=True)
    table = db.Column(db.SmallInteger, default=TableType.none.value)
    user = db.Column(db.String(64), default='')
    email = db.Column(db.String(128), default='')
    text = db.Column(db.String(2048), nullable=False)
    date = db.Column(db.DateTime, default=datetime.now())
    ip = db.Column(db.String(64), default='')
    read = db.Column(db.Boolean, default=False)

    def __init__(self, table, user, email, text, date, ip):
        self.table = table
        self.user = user
        self.email = email
        self.text = text
        self.date = date
        self.ip = ip

    __mapper_args__ = {
        'polymorphic_on': table,
        'polymorphic_identity': 'comment',
        'with_polymorphic': '*'
    }


class EntryComment(Comment):
    entry = db.Column(db.Integer, db.ForeignKey(Entry.id))
    __mapper_args__ = {'polymorphic_identity': TableType.entry.value}

    def __init__(self, table, user, email, text, date, ip, entry):
        super().__init__(table, user, email, text, date, ip)
        self.entry = entry


class ExhibitComment(Comment):
    exhibit = db.Column(db.Integer, db.ForeignKey(Exhibit.id))
    __mapper_args__ = {'polymorphic_identity': TableType.exhibit.value}

    def __init__(self, table, user, email, text, date, ip, exhibit):
        super().__init__(table, user, email, text, date, ip)
        self.exhibit = exhibit


class ProjectComment(Comment):
    project = db.Column(db.Integer, db.ForeignKey(Project.id))
    __mapper_args__ = {'polymorphic_identity': TableType.project.value}

    def __init__(self, table, user, email, text, date, ip, project):
        super().__init__(table, user, email, text, date, ip)
        self.project = project

# functions


def escape(string):
    string = string.replace('\r', '')
    string = string.replace('<', '&lt;')
    string = string.replace('>', '&gt;')
    string = string.replace('"', '&quot;')
    string = string.replace('\n', '<br />')
    string = Markup(string)
    return string

# routes


@app.route('/robots.txt')
def robots():
    return redirect('static/robots.txt')


@app.route('/sitemap.xml')
def sitemap():
    # https://www.quora.com/How-can-I-break-a-Python-Flask-sitemap-into-two-pages
    root = request.url_root[:-1]
    all_rules = app.url_map.iter_rules()
    rules = []
    for rule in all_rules:
        rule_str = str(rule)
        if "/<" in rule_str:
            # Get the Last Slash (/) Index
            lsi = rule_str.rfind('/')
            # Get the Previous Slash Index
            psi = rule_str[:lsi].rfind('/')
            # Get the page (everything before the name) and
            # name (the substring before /<something>)
            page = str(rule_str[:psi])
            name = str(rule_str[psi+1:lsi])

            # Fill a list of subpages with the respective database items
            subpages = []
            if 'blog' in name:
                subpages = Entry.query.all()
            elif 'gallery' in name:
                subpages = Exhibit.query.all()
            elif 'projects' in name:
                subpages = Project.query.all()

            # Add rules by interpreting the item values
            for subpage in subpages:
                target = ''
                date = None
                target = subpage.title.lower().replace(' ', '-')
                date = str(subpage.date)

                # Note that we only want existing targets
                if len(target) > 0:
                    subrule = page + '/' + name + '/' + target
                    rules.append({'url': subrule, 'date': date})
        else:
            rules.append({'url': rule_str})
    sitemap = render_template('sitemap.xml', root=root, rules=rules)
    response = make_response(sitemap)
    response.headers['Content-Type'] = 'application/xml'
    return response


@app.route('/about' or '/about/this')
def this():
    return render_template('about/this.html', title='About this Website',
                           year=app.year)


@app.route('/' or '/read/news', defaults={'page': 1})
@app.route('/read/news/<int:page>', methods=['GET', 'POST'])
def news(page):
    records = Record.query.order_by(Record.date.desc()).all()  # .limit(10)
    news = []
    for record in records:
        text = Markup(record.text)
        date = record.date.date()
        if record.date.date() == datetime.now().date():
            date = 'Today'
        news.append({'text': text, 'date': date})

        total = len(news)
        start = (page-1) * App['per_page']
        end = start + App['per_page']
        current = None
        if end > total:
            current = news[start:]
        else:
            current = news[start:end]
        pagination = Pagination(page, App['per_page'], total)

    return render_template('read/news.html',
                           title='News', year=app.year, news=current,
                           page=page, pagination=pagination)


@app.route('/read/blog', defaults={'which': None, 'page': None})
@app.route('/read/blog/<which>', defaults={'page': 1}, methods=['GET', 'POST'])
@app.route('/read/blog/<which>/<int:page>', methods=['GET', 'POST'])
def blog(which, page):
    if which is None:
        entries = Entry.query.order_by(Entry.date.desc()).all()  # .limit(10)

        for entry in entries:
            # title = Markup(entry.title)
            # date = entry.date.date()
            if entry.date.date() == datetime.now().date():
                entry.date = 'Today at ' + str(entry.date.time())
            else:
                entry.date = str(entry.date.date()) + ' at ' + \
                             str(entry.date.time())
        return render_template('read/blog.html',
                               title='Blog', year=app.year, blog=entries)
    else:
        entry = Entry.query.filter_by(title=which).first()

        if entry is None:
            # If we can't find it, try replacing the '-' with ' '
            entry = Entry.query \
                    .filter_by(title=which.replace('-', ' ')).first()
        if entry is None:
            # If it's still missing it doesn't exist (in the database)
            return render_template('nomatch.min.html',
                                   title='Blog Entry', which=which,
                                   previous='blog')
        entry.views += 1

        if request.method == 'POST':
            sender = ''
            message = ''

            if 'sender' in request.form:
                sender = str(request.form.get('sender'))
            if 'message' in request.form:
                message = str(request.form.get('message'))
                message = escape(message)

            checkToken(request.form.get('other'))

            if Challenge.validate(request.form) is True:
                comment = EntryComment(table=TableType.entry.value,
                                       user=getUser(sender),
                                       email=getEmail(sender),
                                       text=message,
                                       date=datetime.now(),
                                       ip=getIP(request),
                                       entry=entry.id)
                db.session.add(comment)

            clearToken()

        db.session.commit()
        comments = EntryComment.query.filter_by(table=TableType.entry.value,
                                                entry=entry.id) \
            .order_by(EntryComment.date.desc()).all()

        total = len(comments)
        start = (page-1) * App['per_page']
        end = start + App['per_page']
        current = None
        if end > total:
            current = comments[start:]
        else:
            current = comments[start:end]
        pagination = Pagination(page, App['per_page'], total)

        if entry is not None:
            page_str = ''
            if page is not None and page > 1:
                page_str = '/' + str(page)

            token = makeToken()

            return render_template('read/entry/' + str(which) + '.html',
                                   url='/read/blog/' + which + page_str,
                                   page=page, pagination=pagination,
                                   previous='blog',
                                   title=entry.title, year=app.year,
                                   entry=entry, comments=current,
                                   query=Markup(Challenge.get(token)))


@app.route('/watch/gallery', defaults={'which': None, 'page': None})
@app.route('/watch/gallery/<which>', defaults={'page': 1},
           methods=['GET', 'POST'])
@app.route('/watch/gallery/<which>/<int:page>', methods=['GET', 'POST'])
def gallery(which, page):
    if which is None:
        gallery = Exhibit.query.all()
        return render_template('watch/gallery.html', title='Gallery',
                               year=app.year, gallery=gallery)
    else:
        exhibit = Exhibit.query.filter_by(title=which).first()
        if exhibit is None:
            # If we can't find it, try replacing the '-' with ' '
            exhibit = Exhibit.query.filter_by(title=which.replace('-', ' ')) \
                      .first()
        if exhibit is None:
            # If it's still missing it doesn't exist (in the database)
            return render_template('nomatch.min.html',
                                   title='Gallery', which=which)
        exhibit.views += 1

        if request.method == 'POST':
            sender = ''
            message = ''

            if 'sender' in request.form:
                sender = str(request.form.get('sender'))
            if 'message' in request.form:
                message = str(request.form.get('message'))
                message = escape(message)

            checkToken(request.form.get('other'))

            if Challenge.validate(request.form) is True:
                comment = ExhibitComment(table=TableType.exhibit.value,
                                         user=getUser(sender),
                                         email=getEmail(sender),
                                         text=message,
                                         date=datetime.now(),
                                         ip=getIP(request),
                                         exhibit=exhibit.id)
                db.session.add(comment)

            clearToken()

        db.session.commit()
        comments = ExhibitComment.query.filter_by(table=TableType.exhibit
                                                  .value, exhibit=exhibit.id) \
            .order_by(ExhibitComment.date.desc()).all()

        total = len(comments)
        start = (page-1) * App['per_page']
        end = start + App['per_page']
        current = None
        if end > total:
            current = comments[start:]
        else:
            current = comments[start:end]
        pagination = Pagination(page, App['per_page'], total)

        if exhibit.description is not None:
            exhibit.description = Markup(exhibit.description)

        page_str = ''
        if page is not None and page > 1:
            page_str = '/' + str(page)

        token = makeToken()

        return render_template('watch/exhibit.html',
                               url='/watch/gallery/'+which+page_str,
                               title=exhibit.title, year=app.year,
                               page=page, pagination=pagination,
                               previous='gallery',
                               exhibit=exhibit, comments=current,
                               query=Markup(Challenge.get(token)))


@app.route('/watch/projects', defaults={'which': None, 'page': None})
@app.route('/watch/projects/<which>', defaults={'page': 1},
           methods=['GET', 'POST'])
@app.route('/watch/projects/<which>/<int:page>', methods=['GET', 'POST'])
def projects(which, page):
    if which is None:
        projects = Project.query.all()
        return render_template('watch/projects.html', title='Projects',
                               year=app.year, projects=projects)
    else:
        project = Project.query.filter_by(title=which).first()
        if project is None:
            # If we can't find it, try replacing the '-' with ' '
            project = Project.query.filter_by(title=which.replace('-', ' ')) \
                .first()
        if project is None:
            # If it's still missing it doesn't exist (in the database)
            return render_template('nomatch.min.html',
                                   title='Project', which=which)
        project.views += 1

        if request.method == 'POST':
            sender = ''
            message = ''

            if 'sender' in request.form:
                sender = str(request.form.get('sender'))
            if 'message' in request.form:
                message = str(request.form.get('message'))
                message = escape(message)

            checkToken(request.form.get('other'))

            if Challenge.validate(request.form) is True:
                comment = ProjectComment(table=TableType.project.value,
                                         user=getUser(sender),
                                         email=getEmail(sender),
                                         text=message,
                                         date=datetime.now(),
                                         ip=getIP(request),
                                         project=project.id)
                db.session.add(comment)

            clearToken()

        db.session.commit()
        comments = ProjectComment.query \
            .filter_by(table=TableType.project.value, project=project.id) \
            .order_by(ProjectComment.date.desc()).all()

        total = len(comments)
        start = (page-1) * App['per_page']
        end = start + App['per_page']
        current = None
        if end > total:
            current = comments[start:]
        else:
            current = comments[start:end]
        pagination = Pagination(page, App['per_page'], total)

        if project.description is not None:
            project.description = Markup(project.description)

        page_str = ''
        if page is not None and page > 1:
            page_str = '/' + str(page)

        token = makeToken()

        return render_template('watch/project.html',
                               url='/watch/projects/'+which+page_str,
                               title=project.title, year=app.year,
                               page=page, pagination=pagination,
                               previous='projects',
                               project=project, comments=current,
                               query=Markup(Challenge.get(token)))


@app.route('/about/me')
def details():
    return render_template('about/me.html', title='About Me', year=app.year)


@app.route('/about/contact', methods=['GET', 'POST'])
def contact():
    if request.method == 'POST':
        subject = ''
        message = ''
        name = ''
        email = smtp['anonymous']

        if 'subject' in request.form:
            subject = str(request.form.get('subject'))
        if 'message' in request.form:
            message = str(request.form.get('message'))
            message = escape(message)
        if 'sender' in request.form:
            name = str(request.form.get('sender'))

            # Check for the Name (Email) pattern
            match = re.match(Pattern['name+email'], name)
            if match is not None:
                name = match.group('name')
                email = match.group('email')
            else:
                # Check for the Email pattern
                match = re.match(Pattern['email'], name)
                if match is not None:
                    email = match.group('email')

        message_text = "Dear Vítor,\n"
        if len(subject) > 0:
            message_text += "I'm sending this message to talk about " \
                + subject + ".\n"
        message_text += "\n" + message + "\n"
        message_text += '\nSincerely,\n' + name + '.'

        try:
            checkToken()

            if Challenge.validate(request.form) is True:
                message = Message(subject=subject, body=message_text,
                                  sender=email, recipients=smtp['recipients'])
                mail.send(message)
        except:
            return render_template('about/sent.html', title='Contact Me',
                                   year=app.year, name=None)

        clearToken()

        return render_template('about/sent.html', title='Contact Me',
                               year=app.year, name=name)
    else:
        token = makeToken()

        return render_template('about/contact.html', title='Contact Me',
                               year=app.year,
                               query=Markup(Challenge.get(token)))


@app.route('/hotlink')
def hotlink():
    return render_template('hotlink.min.html', title='Hotlink', year=app.year)


@app.errorhandler(403)
@app.errorhandler(404)
@app.errorhandler(500)
def notFound(error):
    return render_template('error.min.html', error=error), 404


# @app.before_request
# def generateToken():
#     App['token'] = makeToken()
#     # app.jinja.env.globals['token'] = makeToken()


# def verifyToken():
#     if request.method == "POST" and request is not None:
#         raise Exception(request.form.values)
#         # if request is not None and 'other' in request.form:
#         #     checkToken(request.form.get('other'))
