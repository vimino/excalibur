#! /usr/bin/env python
# -*- coding:utf8 -*-

import sys
import os
path = os.path.join(os.environ['HOME'], 'excalibur', 'app')

sys.path.insert(0,path)
os.chdir(path)

from excalibur import app as application
